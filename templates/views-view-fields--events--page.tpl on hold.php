<div class="views-row-inner">

<div class="events-title"><?php print $fields['title']->content; ?></div>

<div class="event-information">
  <div class="events-body"><?php print $fields['body']->content; ?></div>
  <div class="event-reginfo">
    <div class="event-reginfo-inner">
      <div class="event-date"><?php print $fields['field_event_time_value']->content; ?></div>
	  <div class="event-reglink"><?php print $fields['field_registration_link_url']->content; ?></div>
	</div><!-- /.event-reginfo-inner -->
  </div><!-- /.event_reginfo -->
</div><!-- / .event_information -->
</div><!-- /.views-row-inner -->